package com.druc.mateusz.recruitmenttaskdemoapp.common

import org.junit.Assert.assertEquals
import org.junit.Test

class OrderDataHelperTest {

    @Test
    fun extractingUrlFromEndOfString_singleUrl() {
        val text = "Some text.\thttps://google.com"
        val expectedResult = "https://google.com"
        assertEquals(expectedResult, text.extractUrlFromEnd())
    }

    @Test
    fun extractingUrlFromEndOfString_multipleUrls() {
        val text = "Some description with website url inside https://www.amazon.pl/ and url on end.\thttps://google.com"
        val expectedResult = "https://google.com"
        assertEquals(expectedResult, text.extractUrlFromEnd())
    }

    @Test
    fun extractingUrlFromEndOfString_multipleUrlsWithDelimiters() {
        val text = "Some description with website url inside \thttps://www.amazon.pl/ and url on end.\thttps://google.com"
        val expectedResult = "https://google.com"
        assertEquals(expectedResult, text.extractUrlFromEnd())
    }

    @Test
    fun removingUrlFromEndOfString_singleUrl() {
        val text = "Some text.\thttps://google.com"
        val expectedResult = "Some text."
        assertEquals(expectedResult, text.removeUrlFromEnd())
    }

    @Test
    fun removingUrlFromEndOfString_multipleUrls() {
        val text = "Some description with website url inside https://www.amazon.pl/ and url on end.\thttps://google.com"
        val expectedResult = "Some description with website url inside https://www.amazon.pl/ and url on end."
        assertEquals(expectedResult, text.removeUrlFromEnd())
    }

    @Test
    fun removingUrlFromEndOfString_multipleUrlsWithDelimiters() {
        val text = "Some description with website url inside \thttps://www.amazon.pl/ and url on end.\thttps://google.com"
        val expectedResult = "Some description with website url inside \thttps://www.amazon.pl/ and url on end."
        assertEquals(expectedResult, text.removeUrlFromEnd())
    }

    @Test
    fun parsingStringToDate() {
        val text = "2020-05-26"
        val expectedResult = "Tue May 26 00:00:00 CEST 2020"
        assertEquals(expectedResult, text.toDate().toString())
    }

    @Test
    fun parsingDateToString() {
        val rawDateText = "2020-05-26"
        val dateString = "Tue May 26 00:00:00 CEST 2020"
        val expectedResult = "2020-05-26"

        val date = rawDateText.toDate()
        assertEquals(dateString, date.toString())
        assertEquals(expectedResult, date?.toFormattedString().orEmpty())
    }
}