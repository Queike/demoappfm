package com.druc.mateusz.recruitmenttaskdemoapp.common

import com.druc.mateusz.recruitmenttaskdemoapp.api.model.OrderApiEntity
import com.druc.mateusz.recruitmenttaskdemoapp.database.entities.OrderDbEntity
import com.druc.mateusz.recruitmenttaskdemoapp.domain.model.Order
import org.junit.Assert.assertEquals
import org.junit.Test

class OrdersModelMapperTest {

    @Test
    fun mappingOrdersApiEntitiesToDbEntities() {
        val ordersApiEntities = listOf<OrderApiEntity>(
            OrderApiEntity(
                description = "Some description.\thttps://google.com",
                imageUrl = "https://www.publicdomainpictures.net/pictures/270000/velka/sunset-picture.jpg",
                modificationDate = "2021-04-21",
                orderId = 0,
                title = "Some title"
            ),
            OrderApiEntity(
                description = "Some description with website url inside https://www.amazon.pl/ and url on end.\thttps://google.com",
                imageUrl = "https://www.publicdomainpictures.net/pictures/270000/velka/sunset-picture.jpg",
                modificationDate = "2012-12-24",
                orderId = 1,
                title = "Some title 2"
            )
        )

        val expectedResult = listOf<OrderDbEntity>(
            OrderDbEntity(
                itemId = 0,
                title = "Some title",
                description = "Some description.",
                imageUrl = "https://www.publicdomainpictures.net/pictures/270000/velka/sunset-picture.jpg",
                modificationDate = "2021-04-21".toDate(),
                websiteUrl = "https://google.com"
            ),
            OrderDbEntity(
                itemId = 1,
                title = "Some title 2",
                description = "Some description with website url inside https://www.amazon.pl/ and url on end.",
                imageUrl = "https://www.publicdomainpictures.net/pictures/270000/velka/sunset-picture.jpg",
                modificationDate = "2012-12-24".toDate(),
                websiteUrl = "https://google.com"
            )
        )

        val result = ordersApiEntities.toDbEntities()
        assertEquals(expectedResult, result)
    }

    @Test
    fun mappingDbEntitiesToDomainOrders() {
        val ordersDbEntities = listOf<OrderDbEntity>(
            OrderDbEntity(
                description = "Some description.",
                imageUrl = "https://www.publicdomainpictures.net/pictures/270000/velka/sunset-picture.jpg",
                modificationDate = "2021-04-21".toDate(),
                itemId = 0,
                title = "Some title",
                websiteUrl = "https://google.com"
            ),
            OrderDbEntity(
                description = "Some description with website url inside https://www.amazon.pl/ and url on end.",
                imageUrl = "https://www.publicdomainpictures.net/pictures/270000/velka/sunset-picture.jpg",
                modificationDate = "2012-12-24".toDate(),
                itemId = 1,
                title = "Some title 2",
                websiteUrl = "https://google.com"
            )
        )

        val expectedResult = listOf<Order>(
            Order(
                orderId = 0,
                title = "Some title",
                description = "Some description.",
                imageUrl = "https://www.publicdomainpictures.net/pictures/270000/velka/sunset-picture.jpg",
                modificationDate = "2021-04-21".toDate()?.toFormattedString("dd MMM yyyy"),
                websiteUrl = "https://google.com"
            ),
            Order(
                orderId = 1,
                title = "Some title 2",
                description = "Some description with website url inside https://www.amazon.pl/ and url on end.",
                imageUrl = "https://www.publicdomainpictures.net/pictures/270000/velka/sunset-picture.jpg",
                modificationDate = "2012-12-24".toDate()?.toFormattedString("dd MMM yyyy"),
                websiteUrl = "https://google.com"
            )
        )

        val result = ordersDbEntities.toDomain()
        assertEquals(expectedResult, result)
    }

    @Test
    fun mappingOrdersApiEntitiesToDomainOrders() {
        val ordersApiEntities = listOf<OrderApiEntity>(
            OrderApiEntity(
                description = "Some description.\thttps://google.com",
                imageUrl = "https://www.publicdomainpictures.net/pictures/270000/velka/sunset-picture.jpg",
                modificationDate = "2021-04-21",
                orderId = 0,
                title = "Some title"
            ),
            OrderApiEntity(
                description = "Some description with website url inside https://www.amazon.pl/ and url on end.\thttps://google.com",
                imageUrl = "https://www.publicdomainpictures.net/pictures/270000/velka/sunset-picture.jpg",
                modificationDate = "2012-12-24",
                orderId = 1,
                title = "Some title 2"
            )
        )

        val expectedResult = listOf<Order>(
            Order(
                orderId = 0,
                title = "Some title",
                description = "Some description.",
                imageUrl = "https://www.publicdomainpictures.net/pictures/270000/velka/sunset-picture.jpg",
                modificationDate = "2021-04-21".toDate()?.toFormattedString(DATE_DISPLAYED_FORMAT),
                websiteUrl = "https://google.com"
            ),
            Order(
                orderId = 1,
                title = "Some title 2",
                description = "Some description with website url inside https://www.amazon.pl/ and url on end.",
                imageUrl = "https://www.publicdomainpictures.net/pictures/270000/velka/sunset-picture.jpg",
                modificationDate = "2012-12-24".toDate()?.toFormattedString(DATE_DISPLAYED_FORMAT),
                websiteUrl = "https://google.com"
            )
        )

        val result = ordersApiEntities.toDbEntities().toDomain()
        assertEquals(expectedResult, result)
    }

    companion object {
        private const val DATE_DISPLAYED_FORMAT = "dd MMM yyyy"
    }
}