package com.druc.mateusz.recruitmenttaskdemoapp.common

sealed class Result<T> {
    class Success<T>(val response: T) : Result<T>()
    data class Failure<T>(val errorMessage: String) : Result<T>()
}