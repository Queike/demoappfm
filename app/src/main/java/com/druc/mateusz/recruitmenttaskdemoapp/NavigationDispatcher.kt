package com.druc.mateusz.recruitmenttaskdemoapp

import android.app.Activity
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.navigation.*
import com.druc.mateusz.recruitmenttaskdemoapp.ui.mainList.MainListFragmentDirections
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class NavigationDispatcher @Inject constructor() {

    private val _navigationLiveData: MutableLiveData<NavigationEvent> by lazy { MutableLiveData<NavigationEvent>() }
    val navigationLiveData: LiveData<NavigationEvent> by lazy { _navigationLiveData }

    fun navigateToDetails(websiteUrl: String) {
        val action = MainListFragmentDirections.actionMainListFragmentToItemDetailsFragment(websiteUrl)
        _navigationLiveData.value = NavigationEvent.Action { navigateTo(action) }
    }

    private fun navigateTo(navDirections: NavDirections) {
        _navigationLiveData.value = NavigationEvent.Action {
            val navController = Navigation.findNavController(it, R.id.fragmentContainerView)
            navController.navigateSafe(navDirections)
        }
    }

    private fun NavController.navigateSafe(
        navDirections: NavDirections
    ) {
        val action = currentDestination?.getAction(navDirections.actionId)
        if (action != null) navigate(navDirections)
    }

    fun setNoAction() {
        _navigationLiveData.value = NavigationEvent.Empty
    }

}

sealed class NavigationEvent {
    object Empty : NavigationEvent()
    data class Action(val action: NavigationAction) : NavigationEvent()
}

typealias NavigationAction = (Activity) -> Unit