package com.druc.mateusz.recruitmenttaskdemoapp

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel

abstract class BaseViewModel: ViewModel() {

    val errorLiveData = MutableLiveData<String>()

    fun clearErrorLiveData() {
        errorLiveData.value = ""
    }
}