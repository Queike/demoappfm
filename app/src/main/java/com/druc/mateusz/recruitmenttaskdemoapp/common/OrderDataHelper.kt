package com.druc.mateusz.recruitmenttaskdemoapp.common

import androidx.core.util.PatternsCompat
import java.text.SimpleDateFormat
import java.util.*

fun String.extractUrlFromEnd(delimiter: String = "\t"): String = this.split(delimiter).lastOrNull { PatternsCompat.WEB_URL.matcher(it).find() }.orEmpty()

fun String.removeUrlFromEnd(delimiter: String = "\t") = this.removeSuffix(this.extractUrlFromEnd()).removeSuffix(delimiter)

fun String.toDate(dateFormat: String = "yyyy-MM-dd"): Date? = SimpleDateFormat(dateFormat, Locale.getDefault()).parse(this)

fun Date.toFormattedString(dateFormat: String = "yyyy-MM-dd"): String = SimpleDateFormat(dateFormat, Locale.getDefault()).format(this)