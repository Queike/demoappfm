package com.druc.mateusz.recruitmenttaskdemoapp.common

import android.content.Context
import android.webkit.WebResourceError
import com.druc.mateusz.recruitmenttaskdemoapp.R
import timber.log.Timber
import java.lang.Exception
import java.net.SocketTimeoutException
import java.net.UnknownHostException
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class ErrorMessageProvider @Inject constructor(
    private val context: Context
) {
    fun getMessageForApiException(exception: Exception): String {
        Timber.e(exception)
        return when (exception) {
            is SocketTimeoutException -> context.getString(R.string.socket_timeout_exception_message)
            is UnknownHostException -> context.getString(R.string.unknown_host_exception_message)
            else -> getUnknownErrorMessage(exception.message)
        }
    }

    fun getMessageForDbException(exception: Exception): String {
        return when (exception) {
            is IllegalAccessException -> context.getString(R.string.db_access_error_message)
            else -> context.getString(R.string.unknown_db_error_message)
        }
    }

    fun getMessageForWebClientException(error: WebResourceError?): String {
        return when (val errorDescription = error?.description.toString()) {
            "net::ERR_NAME_NOT_RESOLVED" -> context.getString(R.string.web_error_name_not_resolved_message)
            "net::ERR_FAILED" -> context.getString(R.string.web_error_failed_message)
            "net::ERR_CONNECTION_REFUSED" -> context.getString(R.string.web_error_connection_refused_message)
            else -> getUnknownErrorMessage(errorDescription)
        }
    }

    private fun getUnknownErrorMessage(message: String?) = context.getString(R.string.unknown_error_message, message)
}