package com.druc.mateusz.recruitmenttaskdemoapp.ui.itemDetails

import android.webkit.*
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.druc.mateusz.recruitmenttaskdemoapp.BaseViewModel
import com.druc.mateusz.recruitmenttaskdemoapp.common.ErrorMessageProvider
import javax.inject.Inject

class ItemDetailsViewModel @Inject constructor(
    private val errorMessageProvider: ErrorMessageProvider
) : BaseViewModel() {

    private val _isLoadingLiveData = MutableLiveData<Boolean>().apply { value = true }
    val isLoadingLiveData: LiveData<Boolean>
        get() = _isLoadingLiveData

    val webViewClient = object: WebViewClient() {
        override fun onReceivedError(
            view: WebView?,
            request: WebResourceRequest?,
            error: WebResourceError?
        ) {
            super.onReceivedError(view, request, error)
            errorLiveData.value = errorMessageProvider.getMessageForWebClientException(error)
            _isLoadingLiveData.value = false
        }

        override fun onPageFinished(view: WebView?, url: String?) {
            super.onPageFinished(view, url)
            _isLoadingLiveData.value = false
        }
    }
}