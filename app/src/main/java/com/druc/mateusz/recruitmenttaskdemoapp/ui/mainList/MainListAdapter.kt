package com.druc.mateusz.recruitmenttaskdemoapp.ui.mainList

import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import com.druc.mateusz.recruitmenttaskdemoapp.BaseViewHolder
import com.druc.mateusz.recruitmenttaskdemoapp.domain.model.Order

class MainListAdapter(
    private val onClick: (websiteUrl: String) -> Unit
) : ListAdapter<Order, BaseViewHolder<Order>>(DiffCallback()) {
    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): BaseViewHolder<Order> {
        return MainListItemViewHolder(parent, onClick)
    }

    override fun onBindViewHolder(holder: BaseViewHolder<Order>, position: Int) {
        holder.bind(getItem(position))
    }
}

class DiffCallback: DiffUtil.ItemCallback<Order>() {

    override fun areItemsTheSame(
        oldItem: Order,
        newItem: Order
    ): Boolean {
        return oldItem.orderId == newItem.orderId
    }

    override fun areContentsTheSame(
        oldItem: Order,
        newItem: Order
    ): Boolean = oldItem == newItem
}