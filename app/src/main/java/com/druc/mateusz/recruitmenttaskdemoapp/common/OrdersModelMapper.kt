package com.druc.mateusz.recruitmenttaskdemoapp.common

import com.druc.mateusz.recruitmenttaskdemoapp.api.model.OrderApiEntity
import com.druc.mateusz.recruitmenttaskdemoapp.database.entities.OrderDbEntity
import com.druc.mateusz.recruitmenttaskdemoapp.domain.model.Order

fun List<OrderApiEntity>.toDbEntities(): List<OrderDbEntity> =
    this.map {
        OrderDbEntity(
            itemId = it.orderId,
            title = it.title,
            description = it.description.removeUrlFromEnd(),
            imageUrl = it.imageUrl,
            modificationDate = it.modificationDate.toDate(),
            websiteUrl = it.description.extractUrlFromEnd()
        )
    }

fun List<OrderDbEntity>.toDomain(): List<Order> =
    this.map {
        Order(
            orderId = it.itemId,
            title = it.title,
            description = it.description,
            imageUrl = it.imageUrl,
            modificationDate = it.modificationDate?.toFormattedString("dd MMM yyyy"),
            websiteUrl = it.websiteUrl
        )
    }