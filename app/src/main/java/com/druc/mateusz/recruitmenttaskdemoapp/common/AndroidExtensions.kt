package com.druc.mateusz.recruitmenttaskdemoapp.common

import android.content.Context
import android.widget.ImageView
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.LiveData
import androidx.lifecycle.Observer
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions
import com.bumptech.glide.request.RequestOptions
import com.druc.mateusz.recruitmenttaskdemoapp.R

inline fun <reified T> LiveData<T>.observeChanges(lifecycleOwner: LifecycleOwner, crossinline observer: (T) -> Unit) {
    observe(lifecycleOwner, Observer { observer(it) })
}

fun ImageView.loadFromUrl(imageUrl: String, viewLifecycleOwner: Context) {
    if (imageUrl.isNotEmpty()) {
        val requestOptions = RequestOptions()
            .diskCacheStrategy(DiskCacheStrategy.ALL)

        Glide
            .with(viewLifecycleOwner)
            .load(imageUrl)
            .apply(requestOptions)
            .placeholder(R.drawable.ic_image_placeholder)
            .error(R.drawable.ic_broken_image_placeholder)
            .thumbnail(0.3f)
            .transition(DrawableTransitionOptions.withCrossFade())
            .apply(RequestOptions.circleCropTransform())
            .into(this)
    }
}