package com.druc.mateusz.recruitmenttaskdemoapp.ui.itemDetails

import android.annotation.SuppressLint
import android.os.Bundle
import android.view.View
import com.druc.mateusz.recruitmenttaskdemoapp.BaseFragment
import com.druc.mateusz.recruitmenttaskdemoapp.R
import com.druc.mateusz.recruitmenttaskdemoapp.databinding.FragmentItemDetailsBinding
import com.druc.mateusz.recruitmenttaskdemoapp.di.appComponent

class ItemDetailsFragment : BaseFragment<FragmentItemDetailsBinding, ItemDetailsViewModel>(
    FragmentItemDetailsBinding::inflate
) {

    private val viewModel by viewModel<ItemDetailsViewModel>()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        appComponent.inject(this)
        binding.lifecycleOwner = viewLifecycleOwner
        binding.viewModel = viewModel
        observeErrors(viewModel.errorLiveData, viewModel::clearErrorLiveData)
        setupWebView()
    }

    @SuppressLint("SetJavaScriptEnabled")
    private fun setupWebView() {
        binding.webViewClient = viewModel.webViewClient
        binding.webView.settings.javaScriptEnabled = true
        binding.websiteUrl =
            when (requireContext().resources.getBoolean(R.bool.isLargeScreenDevice)) {
                true -> arguments?.getString(WEBSITE_URL_KEY).orEmpty()
                else -> arguments?.let { ItemDetailsFragmentArgs.fromBundle(it).websiteUrl }.orEmpty()
            }
    }

    companion object {
        private const val WEBSITE_URL_KEY = "WEBSITE_URL_KEY"

        fun getInstance(websiteUrl: String): ItemDetailsFragment {
            val args = Bundle().apply {
                putString(WEBSITE_URL_KEY, websiteUrl)
            }
            return ItemDetailsFragment().apply {
                arguments = args
            }
        }
    }

    override fun onDestroyView() {
        binding.webView.clearCache(true)
        binding.webView.clearHistory()
        binding.webView.destroy()
        super.onDestroyView()
    }

}