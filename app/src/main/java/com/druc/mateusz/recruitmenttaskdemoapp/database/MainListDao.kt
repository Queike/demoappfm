package com.druc.mateusz.recruitmenttaskdemoapp.database

import androidx.room.*
import com.druc.mateusz.recruitmenttaskdemoapp.database.entities.OrderDbEntity

@Dao
interface MainListDao {

    @Transaction
    @Query("SELECT * FROM OrderDbEntity")
    suspend fun getMainListItems(): List<OrderDbEntity>

    @Transaction
    @Query("SELECT * FROM OrderDbEntity WHERE itemId LIKE :itemId")
    suspend fun getMainListItem(itemId: Int): OrderDbEntity

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insert(mainListItemsEntity: List<OrderDbEntity>)

    @Delete
    suspend fun delete(vararg orderDbEntity: OrderDbEntity)
}