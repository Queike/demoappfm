package com.druc.mateusz.recruitmenttaskdemoapp.domain.model

data class Order(
    val orderId: Int,
    val title: String,
    val description: String,
    val imageUrl: String,
    val modificationDate: String?,
    val websiteUrl: String
)