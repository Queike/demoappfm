package com.druc.mateusz.recruitmenttaskdemoapp

import android.app.Application
import com.druc.mateusz.recruitmenttaskdemoapp.di.AppComponent
import com.druc.mateusz.recruitmenttaskdemoapp.di.DaggerAppComponent
import timber.log.Timber

class RecruitmentTaskDemoApp : Application() {

    val appComponent: AppComponent by lazy {
        DaggerAppComponent
            .builder()
            .application(this)
            .build()
    }

    override fun onCreate() {
        super.onCreate()
        Timber.plant(Timber.DebugTree())
    }
}