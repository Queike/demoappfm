package com.druc.mateusz.recruitmenttaskdemoapp.database

import androidx.room.TypeConverter
import com.druc.mateusz.recruitmenttaskdemoapp.common.toDate
import com.druc.mateusz.recruitmenttaskdemoapp.common.toFormattedString
import java.util.*

class DateConverter {

    @TypeConverter
    fun toDate(date: String): Date? {
        return date.toDate()
    }

    @TypeConverter
    fun toDateString(date: Date): String {
        return date.toFormattedString()
    }
}