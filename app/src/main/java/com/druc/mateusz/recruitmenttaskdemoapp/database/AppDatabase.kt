package com.druc.mateusz.recruitmenttaskdemoapp.database

import androidx.room.Database
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import com.druc.mateusz.recruitmenttaskdemoapp.database.entities.OrderDbEntity

@TypeConverters(DateConverter::class)
@Database(entities = [OrderDbEntity::class], version = 1)
abstract class AppDatabase : RoomDatabase() {
    abstract fun mainListDao(): MainListDao

    companion object {
        const val DATABASE_NAME = "recruitment-task-database"
    }
}