package com.druc.mateusz.recruitmenttaskdemoapp.api

import com.druc.mateusz.recruitmenttaskdemoapp.api.model.OrderApiEntity
import retrofit2.http.GET

interface DemoApi {

    @GET("/recruitment-task")
    suspend fun getRecruitmentTaskData(): List<OrderApiEntity>
}