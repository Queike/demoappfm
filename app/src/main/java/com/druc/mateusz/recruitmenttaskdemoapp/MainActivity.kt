package com.druc.mateusz.recruitmenttaskdemoapp

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.druc.mateusz.recruitmenttaskdemoapp.databinding.ActivityMainBinding
import com.druc.mateusz.recruitmenttaskdemoapp.di.appComponent
import javax.inject.Inject

class MainActivity : AppCompatActivity() {

    @Inject
    lateinit var navigationDispatcher: NavigationDispatcher

    private lateinit var binding: ActivityMainBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)
        appComponent.inject(this)
        observeNavigationEvents()
    }

    private fun observeNavigationEvents() {
        navigationDispatcher.navigationLiveData.observe(this) {
            if (it is NavigationEvent.Action) {
                it.action(this)
            }
        }
    }

    override fun onSaveInstanceState(outState: Bundle) {
        navigationDispatcher.setNoAction()
        super.onSaveInstanceState(outState)
    }
}