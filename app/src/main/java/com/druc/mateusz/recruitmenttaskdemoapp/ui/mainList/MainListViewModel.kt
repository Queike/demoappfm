package com.druc.mateusz.recruitmenttaskdemoapp.ui.mainList

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.druc.mateusz.recruitmenttaskdemoapp.BaseViewModel
import com.druc.mateusz.recruitmenttaskdemoapp.NavigationDispatcher
import com.druc.mateusz.recruitmenttaskdemoapp.common.Result
import com.druc.mateusz.recruitmenttaskdemoapp.domain.model.Order
import com.druc.mateusz.recruitmenttaskdemoapp.domain.repository.OrdersRepository
import kotlinx.coroutines.launch
import javax.inject.Inject

class MainListViewModel @Inject constructor(
    private val ordersRepository: OrdersRepository,
    private val navigationDispatcher: NavigationDispatcher
) : BaseViewModel() {

    private val _isLoadingLiveData = MutableLiveData<Boolean>()
    val isLoadingLiveData: LiveData<Boolean>
        get() = _isLoadingLiveData

    private val _listItemsLiveData = MutableLiveData<List<Order>>()
    val listItemsLiveData: LiveData<List<Order>>
        get() = _listItemsLiveData

    init {
        _isLoadingLiveData.value = true
        fetchOrdersData()
    }

    fun fetchOrdersData() {
        viewModelScope.launch {
            when (val fetchedOrdersResult = ordersRepository.fetchOrders()) {
                is Result.Success -> Unit
                is Result.Failure -> errorLiveData.value = fetchedOrdersResult.errorMessage
            }
            loadOrdersData()
        }
    }

    private fun loadOrdersData() {
        viewModelScope.launch {
            when (val loadedOrdersResult = ordersRepository.loadOrders()) {
                is Result.Success -> {
                    _listItemsLiveData.value = loadedOrdersResult.response.sortedBy { it.orderId }
                }
                is Result.Failure -> {
                    errorLiveData.value = loadedOrdersResult.errorMessage
                }
            }
            _isLoadingLiveData.value = false
        }
    }

    fun navigateToItemDetails(websiteUrl: String) {
        navigationDispatcher.navigateToDetails(websiteUrl)
    }
}