package com.druc.mateusz.recruitmenttaskdemoapp.di

import android.app.Activity
import android.app.Application
import android.content.Context
import androidx.fragment.app.Fragment
import com.druc.mateusz.recruitmenttaskdemoapp.MainActivity
import com.druc.mateusz.recruitmenttaskdemoapp.RecruitmentTaskDemoApp
import com.druc.mateusz.recruitmenttaskdemoapp.ui.itemDetails.ItemDetailsFragment
import com.druc.mateusz.recruitmenttaskdemoapp.ui.mainList.MainListFragment
import dagger.BindsInstance
import dagger.Component
import javax.inject.Singleton

@Singleton
@Component(modules = [AppModule::class])
interface AppComponent {

    @Component.Builder
    interface Builder {

        @BindsInstance
        fun application(application: Application): Builder

        fun build(): AppComponent
    }

    fun inject(mainActivity: MainActivity)
    fun inject(mainListFragment: MainListFragment)
    fun inject(itemDetailsFragment: ItemDetailsFragment)
}

val Context.appComponent
    get() = (applicationContext as RecruitmentTaskDemoApp).appComponent

val Fragment.appComponent
    get() = requireContext().appComponent

val Activity.appComponent
    get() = applicationContext.appComponent