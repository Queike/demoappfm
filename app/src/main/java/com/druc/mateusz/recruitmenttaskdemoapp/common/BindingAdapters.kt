package com.druc.mateusz.recruitmenttaskdemoapp.common

import android.content.Context
import android.view.View
import android.webkit.WebView
import android.webkit.WebViewClient
import android.widget.ImageView
import androidx.databinding.BindingAdapter

@BindingAdapter("visibleIf")
fun setVisible(view: View, visible: Boolean) {
    view.visibility = if (visible) View.VISIBLE else View.GONE
}

@BindingAdapter("webViewClient")
fun setWebViewClient(view: WebView, client: WebViewClient) {
    view.webViewClient = client
}

@BindingAdapter("webViewUrlSource")
fun setWebViewUrlSource(view: WebView, url: String) {
    if (url.isNotEmpty()) {
        view.loadUrl(url)
    }
}

@BindingAdapter(value = ["android:src", "context"], requireAll = true)
fun setImageUrl(imageView: ImageView, imageUrl: String, context: Context) {
    if (imageUrl.isNotEmpty()) {
        imageView.loadFromUrl(imageUrl, context)
    }
}