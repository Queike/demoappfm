package com.druc.mateusz.recruitmenttaskdemoapp.ui.mainList

import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import com.druc.mateusz.recruitmenttaskdemoapp.BaseViewHolder
import com.druc.mateusz.recruitmenttaskdemoapp.R
import com.druc.mateusz.recruitmenttaskdemoapp.databinding.LayoutMainListItemBinding
import com.druc.mateusz.recruitmenttaskdemoapp.domain.model.Order
import com.druc.mateusz.recruitmenttaskdemoapp.inflateView

class MainListItemViewHolder(
    parent: ViewGroup,
    private val onClick: (websiteUrl: String) -> Unit
): BaseViewHolder<Order>(parent.inflateView(R.layout.layout_main_list_item)) {

    override fun bind(item: Order) {
        with(itemView) {
            val binding = DataBindingUtil.bind<LayoutMainListItemBinding>(this.rootView)
            binding?.listItem = item
            binding?.itemCardView?.setOnClickListener { onClick.invoke(item.websiteUrl) }
        }
    }
}