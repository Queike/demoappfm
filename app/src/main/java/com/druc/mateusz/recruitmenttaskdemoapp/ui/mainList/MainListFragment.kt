package com.druc.mateusz.recruitmenttaskdemoapp.ui.mainList

import android.os.Bundle
import android.view.View
import androidx.recyclerview.widget.LinearLayoutManager
import com.druc.mateusz.recruitmenttaskdemoapp.BaseFragment
import com.druc.mateusz.recruitmenttaskdemoapp.R
import com.druc.mateusz.recruitmenttaskdemoapp.common.observeChanges
import com.druc.mateusz.recruitmenttaskdemoapp.databinding.FragmentMainListBinding
import com.druc.mateusz.recruitmenttaskdemoapp.di.appComponent
import com.druc.mateusz.recruitmenttaskdemoapp.ui.itemDetails.ItemDetailsFragment

class MainListFragment : BaseFragment<FragmentMainListBinding, MainListViewModel>(FragmentMainListBinding::inflate) {

    private val viewModel by viewModel<MainListViewModel>()
    private val mainListAdapter by lazy { MainListAdapter(::showDetails) }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        appComponent.inject(this)
        binding.lifecycleOwner = viewLifecycleOwner
        binding.viewModel = viewModel
        setupList()
        setupObservers()
    }

    private fun setupList() {
        binding.mainListLayout.swipeRefreshLayout.setOnRefreshListener { viewModel.fetchOrdersData() }
        with(binding.mainListLayout.mainListRecyclerView) {
            this.adapter = mainListAdapter
            this.layoutManager = LinearLayoutManager(requireContext())
        }
    }

    private fun setupObservers() {
        viewModel.listItemsLiveData.observeChanges(viewLifecycleOwner) { listData ->
            binding.mainListLayout.swipeRefreshLayout.isRefreshing = false
            mainListAdapter.submitList(listData)
        }
        observeErrors(viewModel.errorLiveData) { viewModel.clearErrorLiveData() }
    }

    private fun showDetails(websiteUrl: String) {
        when (requireContext().resources.getBoolean(R.bool.isLargeScreenDevice)) {
            true -> childFragmentManager.beginTransaction().add(R.id.itemDetailsFragmentContainerView, ItemDetailsFragment.getInstance(websiteUrl)).commit()
            else -> viewModel.navigateToItemDetails(websiteUrl)
        }
    }

    override fun onDestroyView() {
        binding.mainListLayout.mainListRecyclerView.adapter = null
        super.onDestroyView()
    }

}