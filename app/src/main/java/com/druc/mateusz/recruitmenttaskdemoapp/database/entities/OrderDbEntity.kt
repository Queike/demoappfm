package com.druc.mateusz.recruitmenttaskdemoapp.database.entities

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import java.util.*

@Entity
data class OrderDbEntity(
    @PrimaryKey val itemId: Int,
    @ColumnInfo(name = "title") val title: String,
    @ColumnInfo(name = "description") val description: String,
    @ColumnInfo(name = "image_url") var imageUrl: String,
    @ColumnInfo(name = "modification_date") var modificationDate: Date?,
    @ColumnInfo(name = "website_url") var websiteUrl: String
)