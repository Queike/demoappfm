package com.druc.mateusz.recruitmenttaskdemoapp.domain.repository

import com.druc.mateusz.recruitmenttaskdemoapp.api.DemoApi
import com.druc.mateusz.recruitmenttaskdemoapp.api.model.OrderApiEntity
import com.druc.mateusz.recruitmenttaskdemoapp.common.ErrorMessageProvider
import com.druc.mateusz.recruitmenttaskdemoapp.common.Result
import com.druc.mateusz.recruitmenttaskdemoapp.common.toDbEntities
import com.druc.mateusz.recruitmenttaskdemoapp.common.toDomain
import com.druc.mateusz.recruitmenttaskdemoapp.database.AppDatabase
import com.druc.mateusz.recruitmenttaskdemoapp.domain.model.Order
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import java.lang.Exception
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class OrdersRepository @Inject constructor(
    private val api: DemoApi,
    private val errorMessageProvider: ErrorMessageProvider,
    private val database: AppDatabase
) {

    suspend fun fetchOrders(): Result<Unit> {
        val items: List<OrderApiEntity>

        return withContext(Dispatchers.IO) {
            try {
                items = api.getRecruitmentTaskData()
                database.mainListDao().insert(items.toDbEntities())
                Result.Success(Unit)
            } catch (exception: Exception) {
                Result.Failure(errorMessageProvider.getMessageForApiException(exception))
            }
        }
    }

    suspend fun loadOrders(): Result<List<Order>> {
        return withContext(Dispatchers.IO) {
            try {
                val mainListItems = database.mainListDao().getMainListItems()
                Result.Success<List<Order>>(mainListItems.toDomain())
            } catch (exception: Exception) {
                Result.Failure<List<Order>>(errorMessageProvider.getMessageForDbException(exception))
            }
        }
    }
}