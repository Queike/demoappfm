package com.druc.mateusz.recruitmenttaskdemoapp.di

import android.app.Application
import android.content.Context
import android.content.SharedPreferences
import android.content.res.Resources
import androidx.preference.PreferenceManager
import androidx.room.Room
import com.druc.mateusz.recruitmenttaskdemoapp.api.DemoApi
import com.druc.mateusz.recruitmenttaskdemoapp.BuildConfig
import com.druc.mateusz.recruitmenttaskdemoapp.database.AppDatabase
import com.druc.mateusz.recruitmenttaskdemoapp.database.AppDatabase.Companion.DATABASE_NAME
import dagger.Module
import dagger.Provides
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Singleton

@Module
class AppModule {

    @Singleton
    @Provides
    fun context(application: Application): Context = application.baseContext

    @Singleton
    @Provides
    fun resources(context: Context): Resources = context.resources

    @Singleton
    @Provides
    fun sharedPreferences(context: Context): SharedPreferences {
        return PreferenceManager.getDefaultSharedPreferences(context)
    }

    @Singleton
    @Provides
    fun database(context: Context) = Room.databaseBuilder(
        context,
        AppDatabase::class.java, DATABASE_NAME
    ).build()

    @Provides
    fun rxCallAdapterFactory() = RxJava2CallAdapterFactory.create()

    @Singleton
    @Provides
    fun demoApi(): DemoApi {
        val client = OkHttpClient.Builder()
            .addInterceptor(
                HttpLoggingInterceptor().apply { level = HttpLoggingInterceptor.Level.BODY }
            ).build()
        val retrofit = Retrofit.Builder()
            .baseUrl(BuildConfig.BASE_URL)
            .client(client)
            .addConverterFactory(GsonConverterFactory.create())
            .build()
        return retrofit.create(DemoApi::class.java)
    }
}
