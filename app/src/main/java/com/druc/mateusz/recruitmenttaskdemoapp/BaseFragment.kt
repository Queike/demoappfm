package com.druc.mateusz.recruitmenttaskdemoapp

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.viewbinding.ViewBinding
import com.druc.mateusz.recruitmenttaskdemoapp.common.observeChanges
import javax.inject.Inject

typealias Inflate<T> = (LayoutInflater, ViewGroup?, Boolean) -> T

abstract class BaseFragment<VB: ViewBinding, VM : ViewModel>(
    private val inflate: Inflate<VB>
) : Fragment() {

    @Inject
    lateinit var viewModelFactory: ViewModelFactory<VM>

    private var _binding: VB? = null
    val binding get() = _binding!!

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        _binding = inflate.invoke(inflater, container, false)
        return binding.root
    }

    protected inline fun <reified T : ViewModel> viewModel() = lazy {
        ViewModelProvider(this, viewModelFactory)[T::class.java]
    }

    fun observeErrors(errorLiveData: MutableLiveData<String>, clearError: () -> Unit) {
        errorLiveData.observeChanges(viewLifecycleOwner) { errorMessage ->
            if (errorMessage.isNotEmpty()) {
                Toast.makeText(requireContext(), errorMessage, Toast.LENGTH_LONG).show()
                clearError.invoke()
            }
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}